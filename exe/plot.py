from my_visualization import MyVisualization as MV
import numpy as np
import os, glob

def sortKeyFunc(s):
    return int(os.path.basename(s)[0:7])

files = glob.glob('output/*.txt')
files.sort(key=sortKeyFunc)

vis = MV(aspect=3)

for f in files:
    d = np.loadtxt(f, dtype={'names': ('x', 'rho', 'u', 'p'), 'formats':('f8','f8','f8','f8')})

    vis.plt.cla()
    vis.plt.clf()

    ax_rho = vis.new2daxes(pos='311')
    ax_p = vis.new2daxes(pos='312')
    ax_u = vis.new2daxes(pos='313')

    vis.plot(d['x'], d['rho'], ax=ax_rho, ymin=0, ymax=1)

    vis.plot(d['x'], d['p'], ax=ax_p, ymin=0, ymax=1)

    vis.plot(d['x'], d['u'], ax=ax_u, ymin=0, ymax=1)

    ax_rho.set_ylabel("rho", fontdict={'size': 18})
    ax_p.set_ylabel("p", fontdict={'size': 18})
    ax_u.set_ylabel("u", fontdict={'size': 18})
    ax_u.set_xlabel("x", fontdict={'size': 18})

    vis.save("plots/" + os.path.basename(f) + ".png", transparent=False)
