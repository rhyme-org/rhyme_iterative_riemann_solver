program irs_two_shocks_collision_exe
  use rhyme_iterative_riemann_solver_factory

  implicit none

  type(ideal_gas_t) :: ig
  type(hydro_conserved_t) :: L, R, U
  type(rp_star_region_t) :: star
  type(chemistry_t) :: chemi
  type ( iterative_riemann_solver_config_t ) :: irs_config

  integer :: i, step, n_outputs
  real(kind=8) :: dt, t_final
  character(len=256) :: output_name

  call chemi%init
  call ig%init_with(chemi, igid%diatomic)

  call irs_two_shocks_collision_test (L, R, star)
  call iterative_riemann_solver (ig, L, R, hyid%x, irs_config, star)

  t_final = .2d0
  n_outputs = 128

  dt = t_final / n_outputs

  do step = 1, n_outputs

    write(output_name,'("output/",i0.7,".txt")') step

    open ( unit=10, file=output_name, action='write', form='formatted' )
      do i = 1, 512
        call irs_sampling ( ig, L, R, star, hyid%x, -.5d0 + real(i - 1, kind=8) / 511.d0, step * dt, U )

        write (10, '(F25.12," ",F25.12," ",F25.12," ",F25.12)') &
          (-.5d0 + real(i - 1, kind=8) / 511.d0), &
          u%u(hyid%rho), &
          u%u(hyid%rho_u) / u%u(hyid%rho), &
          ig%p(u)

      end do
    close (10)
  end do

end program irs_two_shocks_collision_exe
